import json
import paho.mqtt.publish as publish
import time
import tools_config
# connact client to topic  hello/world:
#   mosquitto_sub -d -t hello/world
#   mosquitto_sub -d -t topic/setSensorsDigitalOutput
#   mosquitto_sub -d -t topic/publicCommandLog
#   mosquitto_sub -d -t "topic/ServiceStartError"
#   mosquitto_sub -d -t topic/ServiceRuntimeError

set_relay = {}
set_relay["relay_id"] = 1
set_relay["relay_state"] = True
error, topic_config = tools_config.load_config("config_topics.json")

def main():
    # Main program block
    while True:
        if set_relay["relay_state"] == True:
            set_relay["relay_state"] = False
        else:
            set_relay["relay_state"] = True
        # Publishing sensor information by JSON converting object to a string
        publish.single(topic_config["digital_output_set_sensors_topic"], json.dumps(set_relay), hostname = "localhost")
        # Printing JSON objects
        print(set_relay)
        time.sleep(2)

if __name__ == '__main__':

  try:
    main()
  except KeyboardInterrupt:
    pass
  finally:
    exit(0)

