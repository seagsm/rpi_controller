RECONFIGURATION:
See systemctl sevices:
sudo systemctl status

Stop services:
sudo systemctl stop raspmqtttemperaturesensor.service
sudo systemctl stop raspmqtttemperaturetomysql.service

Now we can start change system configuration:

1) Set correct data in config_system.json

    "box_id": "v1_0000000001",         - set actual board id
    "box_username": "mqtt_controller", - notused
    "box_password": "qwertyui",        - not used
    "db_host": "localhost",            - set correct ip of MySql server
    "db_name": "Electronics",          - set correct MySql DB name
    "db_user": "mqtt_user",            - set correct MySql DB user name
    "db_password":"NoHuP129456cz"      - set correct MySql DB user password


2) Connect all used temperature sensors and start test_temperature_sensor.py

python3  test_temperature_sensor.py

Now will be generated sensor list in config_temperature_sensor.json file

3) To detect relation between sensor ID and real sensor place(name of sensor, name of sensor plase) lets start test_temperature.py:
python3  test_temperature.py
You well see somethinf like this:
(Here list for two sensors only)
...
{"28-03168038b6ff": 19.625, "28-04168025b5ff": 24.937}
{"28-03168038b6ff": 19.625, "28-04168025b5ff": 24.75}
{"28-03168038b6ff": 19.625, "28-04168025b5ff": 24.625}
{"28-03168038b6ff": 19.625, "28-04168025b5ff": 24.5}
{"28-03168038b6ff": 19.687, "28-04168025b5ff": 24.5}
...
Cheanging temperature on each sensors you can denect it ID.
You can see, in according this the list sensor with ID "28-04168025b5ff" has temperature 24.5
You must create (or change ) file config_temperature_channels.json, looks like this:
{
    "28-03168038b6ff": "oil_temperature",
    "28-04168025b5ff": "inlet_temperature"
}
It described set of two temperature sensors.

As you can see, each sendor ID has description tag (this tag will be used later for saving channel name to database, so it must be one word, for example:
oil_barrel
control_room
input_valve
  and so on.
)

4) Now we can check how temperature subsistem work:
mqtt server shoud be srarted automaticaly.
Start temperature service script:
python3 service_temperature_sensors.py &

and start MQTT client subscribed to temperature topic:
mosquitto_sub -d -t topic/sensorsTemperature

You should see something like this:
{"28-03168038b6ff": [[19.875, "oil_temperature", "v1_0000000001"]], "28-04168025b5ff": [[19.25, "inlet_temperature", "v1_0000000001"]]}
Client (null) received PUBLISH (d0, q0, r0, m0, 'topic/sensorsTemperature', ... (135 bytes))
{"28-03168038b6ff": [[19.875, "oil_temperature", "v1_0000000001"]], "28-04168025b5ff": [[19.25, "inlet_temperature", "v1_0000000001"]]}
Client (null) received PUBLISH (d0, q0, r0, m0, 'topic/sensorsTemperature', ... (135 bytes))
{"28-03168038b6ff": [[19.875, "oil_temperature", "v1_0000000001"]], "28-04168025b5ff": [[19.25, "inlet_temperature", "v1_0000000001"]]}
For each sensors ID you should see correct name,board ID and temperature value.

kill process with "python3 service_temperature_sensors.py &"
(ps -ax   to see process list )
kill -9 process_number
Or see google, how to.



Start, stop, restart, enable, reload serveces:
sudo systemctl stop raspmqtttemperaturesensor.service
sudo systemctl stop raspmqtttemperaturetomysql.service

sudo systemctl start raspmqtttemperaturesensor.service
sudo systemctl start raspmqtttemperaturetomysql.service

sudo systemctl restart raspmqtttemperaturesensor.service
sudo systemctl restart raspmqtttemperaturetomysql.service

sudo sysremctl daemon-reload

sudo systemctl enable raspmqtttemperaturesensor.service
sudo systemctl enable raspmqtttemperaturetomysql.service


or reboot.


How to check topic:
mosquitto_sub -u mqtt_user -P qwertyui -d -t topic/sensorsDigitalInput

WiFi scan
sudo iwlist wlan0 scan

Choose WiFi and add it to config:
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf 
This will open up wpa_supplicant.conf file. 
It looks like:
network={
    ssid="You SSID Name"
    psk="Your WiFI Password"
    key_mgmt=WPA-PSK
}
Save, reboot.

Command for change user password:
passwd





