#!/usr/bin/python3

import time
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json
import tools_config
import mysql.connector
from mysql.connector import Error
import datetime




set_topic = ""
log_topic = ""
error_runtime_topic = ""

mqtt_auth = { 'username': 'mqtt_user', 'password': 'qwertyui' }


# This is the Subscriber

def on_connect(client, userdata, flags, rc):
#    print("Connected with result code " + str(rc))
    client.subscribe(set_topic)


def on_message(client, userdata, msg):
    local_error = 0
    # read payload:
    try:
        loaded = json.loads(msg.payload)
#        print(loaded)
    except IOError as e:
    	local_error = -1
 

    # parsing payload:
#    local_error = -1

    if local_error == 0:
    #save payload to DB:
        try:
            connection = mysql.connector.connect(host=system_config["db_host"],
                                                 database=system_config["db_name"],
                                                 user=system_config["db_user"],
                                                 password=system_config["db_password"])

#            connection = mysql.connector.connect(host='localhost',
#                                                 database='monitoring_db',
#                                                 user='mqtt_connect_user',
#                                                 password='NoHuP129456cz')



            mySql_insert_query = """INSERT INTO temperature_sensors (board_id, sensor_id, channel_name, value, sensor_state, ts, dt) VALUES (%s, %s, %s, %s, %s, NOW(),NOW()) """
            if connection.is_connected():
                db_Info = connection.get_server_info()
                #print("Connected to MySQL Server version ", db_Info)
                cursor = connection.cursor()
                #cursor.execute("SET time_zone = '+01:00';")
                for local_element in list(loaded.keys()):
                    local_sens_id = local_element
                    local_value = loaded[local_sens_id]
                    local_temperature = 0
                    local_state= "online" 
                    local_temperature_state = local_value[0][0]
                    if type(local_temperature_state) is str:
                        local_state = local_temperature_state
                    else:
                        local_temperature = local_temperature_state
                    #                         board_id         sensor_id     channel_name        value   
                    local_record_value = (local_value[0][2], local_sens_id, local_value[0][1], local_temperature, local_state)

# INSERT INTO temperature_sensors(board_id,sensor_id, channel_name,sensor_state, value, ts,dt) VALUES('board_0001','28-1234567890','sensor_0', 'online', 9.39, NOW(),NOW());
                    cursor.execute(mySql_insert_query, local_record_value)

                    #mySql_insert_query = """INSERT INTO temperature_sensors(board_id,sensor_id, channel_name,sensor_state, value, ts,dt) VALUES('board_0001','28-1234567890','sensor_0', 'online', 9.39, NOW(),NOW()) """
                    #cursor.execute(mySql_insert_query)
                    connection.commit()

        except Error as e:
            print("Error while connecting to MySQL", e)
            local_error = -1 

        if connection.is_connected():
            cursor.close()
            connection.close()
            # print("MySQL connection is closed")


    if local_error == 0:
        publish.single(log_topic, json.dumps(loaded), hostname="localhost", auth=mqtt_auth)
    else:
        publish.single(error_runtime_topic, json.dumps(loaded), hostname="localhost", auth=mqtt_auth)

    #print(json.loads(msg.payload)) #converting the string back to a JSON object


tc_error, topic_config = tools_config.load_config("config_topics.json")
if tc_error == 0:
    cs_error, system_config= tools_config.load_config("config_system.json")
    if cs_error == 0:
        set_topic = ""
        set_topic = topic_config["temperature_sensors_topic"]
        log_topic = topic_config["public_log"]
        error_runtime_topic = topic_config["error_runtime_log"]
        client = mqtt.Client()
        client.on_connect = on_connect
        client.on_message = on_message

        #print(system_config['mqtt_user'])
        #print(system_config['mqtt_password'])

        client.username_pw_set(system_config['mqtt_user'], system_config['mqtt_password'] )
        client.connect("localhost", 1883, 60)
        client.loop_forever()







