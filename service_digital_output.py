#!/usr/bin/python3

import gpiozero
import time
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json
import tools_config

# GPIO LIST  for ext pins:
# LED pins:
# GPIO 6
# GPIO 12
# GPIO 13
# GPIO 16
# Free pins:
# GPIO 19
# GPIO 20
# GPIO 26
# GPIO 21

# Relay pins:
# GPIO 22
# GPIO 27
# GPIO 18
# GPIO 17

# SSR 0
# CH0 GPIO 23
# CH1 GPIO 5
# SSR 1
# CH0 GPIO 24
# CH1 GPIO 25



relay0 = gpiozero.LED(22,active_high=False, initial_value=False)
relay1 = gpiozero.LED(27,active_high=False, initial_value=False)
relay2 = gpiozero.LED(18,active_high=False, initial_value=False)
relay3 = gpiozero.LED(17,active_high=False, initial_value=False)

ssr00 = gpiozero.LED(23,active_high=True, initial_value=False)
ssr01 = gpiozero.LED(5,active_high=True, initial_value=False)
ssr10 = gpiozero.LED(24,active_high=True, initial_value=False)
ssr11 = gpiozero.LED(25,active_high=True, initial_value=False)

set_topic = ""
log_topic = ""
error_runtime_topic = ""

mqtt_auth = { 'username': 'mqtt_user', 'password': 'qwertyui' }

state_load = {}
relay_name = ""


# This is the Subscriber
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe(set_topic)


def on_message(client, userdata, msg):
    local_error = 0
    try:
        loaded = json.loads(msg.payload)
    except IOError as e:
    	local_error = -1
    relay_id = 0

    if "relay_id" in loaded:
        relay_id = loaded["relay_id"]
    else:
        local_error = -1

    if "relay_state" in loaded:
        relay_state = loaded["relay_state"]
    else:
        local_error = -1

    if local_error == 0:
        if relay_id == 1:
            if relay_state:
                relay0.on()
            else:
                relay0.off()
        elif relay_id == 2:
            if relay_state == True:
                relay1.on()
            else:
                relay1.off()
        elif relay_id == 3:
            if relay_state == True:
                relay2.on()
            else:
                relay2.off()
        elif relay_id == 4:
            if relay_state == True:
                relay3.on()
            else:
                relay3.off()
        elif relay_id == 5:
            if relay_state == True:
                ssr00.on()
            else:
                ssr00.off()
        elif relay_id == 6:
            if relay_state == True:
                ssr01.on()
            else:
                ssr01.off()
        elif relay_id == 7:
            if relay_state == True:
                ssr10.on()
            else:
                ssr10.off()
        elif relay_id == 8:
            if relay_state == True:
                ssr11.on()
            else:
                ssr11.off()
        else:
            local_error = -1
    if local_error == 0:
        relay_name = "relay_"+str(relay_id)
        state_load = {relay_name:relay_state}
        publish.single(state_topic, json.dumps(state_load), hostname="localhost", auth=mqtt_auth)
    else:
        publish.single(error_runtime_topic, json.dumps(loaded), hostname="localhost", auth=mqtt_auth)

    #print(json.loads(msg.payload)) #converting the string back to a JSON object


error, topic_config = tools_config.load_config("config_topics.json")
if error == 0:
    set_topic = ""
    set_topic = topic_config["digital_output_set_sensors_topic"]
    state_topic = topic_config["digital_output_state_sensors_topic"]
    error_runtime_topic = topic_config["error_runtime_log"]
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set('mqtt_user', 'qwertyui')
    client.connect("localhost", 1883, 60)

    client.loop_forever()


