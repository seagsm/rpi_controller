import datetime
import logging
import logging.handlers


# logger.info("message")
# logger.debug("message") ...

def init_log():
    now_time = datetime.datetime.now()
    format_time = now_time.strftime("%Y-%m-%d %H:%M:%S")
    format_time = format_time.replace('-','_')
    format_time = format_time.replace(' ', '_')
    format_time = format_time.replace(':', '_')
    file_name = format_time + ".log"
    f = logging.Formatter(fmt='%(levelname)s:%(asctime)s: %(message)s ', datefmt="%Y-%m-%d %H:%M:%S")
    # TimedRotatingFileHandler
    handlers = [
        #logging.handlers.RotatingFileHandler(file_name, encoding='utf8', maxBytes=100000, backupCount=10),
        #logging.handlers.TimedRotatingFileHandler(log_file_name, when="S", interval=30, backupCount=10)

        logging.handlers.TimedRotatingFileHandler(file_name, when="S", interval=86400, backupCount=5, encoding='utf8'),

        #Add if need more console output:
        logging.StreamHandler()
    ]
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    for h in handlers:
        h.setFormatter(f)
        h.setLevel(logging.INFO)
        root_logger.addHandler(h)



