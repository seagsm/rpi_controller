import RPi.GPIO as GPIO
import time
import json
import math
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import os
import glob
import time

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_folder1 = glob.glob(base_dir + '28*')[1]
print(device_folder)
print(device_folder1)
device_file = device_folder + '/w1_slave'
device_file1 = device_folder1 + '/w1_slave'


# This is the Subscriber
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("topic/sensorTemperature")


def on_message(client, userdata, msg):
    loaded = json.loads(msg.payload)
    print(json.loads(msg.payload))  # converting the string back to a JSON object
    print(loaded['temperature_0'])


def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines


def read_temp_raw1():
    f = open(device_file1, 'r')
    lines = f.readlines()
    f.close()
    return lines


def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()

    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos + 2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c


def read_temp1():
    lines = read_temp_raw1()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw1()

    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos + 2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c



temperature = {}
temperature['temperature_0'] = read_temp()
temperature['temperature_1'] = read_temp1()
temperature['sensor'] = 'ds'

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("localhost", 1883, 60)
client.loop_start()


def main():
    # Main program block
    while True:
        # Publishing sensor information by JSON converting object to a string
        publish.single("topic/sensorTemperature", json.dumps(temperature), hostname="localhost")

        # Printing JSON objects
        print(temperature)
        time.sleep(2)


if __name__ == '__main__':

    try:
        main()
    except KeyboardInterrupt:
        pass
    finally:

        GPIO.cleanup()







