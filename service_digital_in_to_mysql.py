import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json
import tools_config
import mysql.connector
from mysql.connector import Error


set_topic = ""
log_topic = ""
error_runtime_topic = ""
mqtt_auth={}

# This is the Subscriber

def on_connect(client, userdata, flags, rc):
    client.subscribe(set_topic)


def on_message(client, userdata, msg):
    local_error = 0
    # read payload:
    try:
        loaded = json.loads(msg.payload)
    #        print(loaded)
    except IOError as e:
        local_error = -1

    if local_error == 0:
        # save payload to DB:
        try:
            connection = mysql.connector.connect(host=system_config["db_host"],
                                                 database=system_config["db_name"],
                                                 user=system_config["db_user"],
                                                 password=system_config["db_password"])
            mySql_insert_query = """INSERT INTO digital_input (board_id, input_number, input_name, value, ts, dt) VALUES (%s, %s, %s, %s, NOW(),NOW()) """
            if connection.is_connected():
                cursor = connection.cursor()
                for local_element in list(loaded.keys()):
                    local_sens_id = local_element
                    local_value = loaded[local_sens_id]
                    local_record_value = (local_value[0][2], local_sens_id, local_value[0][1], local_value[0][0])
                    cursor.execute(mySql_insert_query, local_record_value)
                    connection.commit()

        except Error as e:
            print("Error while connecting to MySQL", e)
            local_error = -1

        if connection.is_connected():
            cursor.close()
            connection.close()

    if local_error == 0:
        publish.single(log_topic, json.dumps(loaded), hostname="localhost", auth=mqtt_auth)
    else:
        publish.single(error_runtime_topic, json.dumps(loaded), hostname="localhost", auth=mqtt_auth)


tc_error, topic_config = tools_config.load_config("config_topics.json")
if tc_error != 0:
    exit(tc_error)
cs_error, system_config = tools_config.load_config("config_system.json")
if cs_error != 0:
    exit(cs_error)

set_topic = ""
set_topic = topic_config["digital_input_sensors_topic"]
log_topic = topic_config["public_log"]
error_runtime_topic = topic_config["error_runtime_log"]

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

mqtt_auth = { 'username': system_config['mqtt_user'], 'password': system_config['mqtt_password'] }

print(system_config['mqtt_user'])
print(system_config['mqtt_password'])

client.username_pw_set(system_config['mqtt_user'], system_config['mqtt_password'])
client.connect("localhost", 1883, 60)
client.loop_forever()








