import gpiozero
import time


relay0 = gpiozero.LED(22,active_high=False, initial_value=False)
relay1 = gpiozero.LED(27,active_high=False, initial_value=False)
relay2 = gpiozero.LED(18,active_high=False, initial_value=False)
relay3 = gpiozero.LED(17,active_high=False, initial_value=False)

ssr00 = gpiozero.LED(23,active_high=True, initial_value=False)
ssr01 = gpiozero.LED(5,active_high=True, initial_value=False)
ssr10 = gpiozero.LED(24,active_high=True, initial_value=False)
ssr11 = gpiozero.LED(25,active_high=True, initial_value=False)


for i in range(100):
    relay0.on()
    relay3.off()
    time.sleep(0.5)

    relay1.on()
    relay0.off()
    time.sleep(0.5)

    relay2.on()
    relay1.off()
    time.sleep(0.5)

    relay3.on()
    relay2.off()
    time.sleep(0.5)


#    ssr00.off()
#    ssr11.on()
#    time.sleep(0.5)

#    ssr01.off()
#    ssr00.on()
#    time.sleep(0.5)

    ssr10.off()
    ssr11.on()
    time.sleep(0.5)

    ssr11.off()
    ssr10.on()
    time.sleep(0.5)














