#!/usr/bin/python3

import gpiozero
import time
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json
import tools_config

mqtt_auth = { 'username': 'mqtt_user', 'password': 'qwertyui' }

# This is the Subscriber
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe(temperature_topic)




def on_message(client, userdata, msg):
    local_error = 0
    try:
        loaded = json.loads(msg.payload)
    except IOError as e:
    	local_error = -1

    if local_error == 0:
        # Each time reload temperature control config file to check On/Off control:
        local_error, temperature_control_config = tools_config.load_config("config_temperature_control.json")
        if local_error != 0:
            print("load config_temperature_control error")

        if local_error == 0:
            # Compare each sensors id from input topic:
            for sensor_id in loaded:
#                print("sensor_id = " + str(sensor_id))
                # with each sensor id from config topic, 
                # it is necessary for case if one temperature sensor ID used for driving several relays:
#                print(len(temperature_control_config))
                for current_case in temperature_control_config:
                    case_numbers = len(temperature_control_config)
                    config_sensor_id = ""
                    if case_numbers > 0:
                        config_sensor_id = temperature_control_config[current_case]["temperature_sensor_id"]
#                   print("config_sensor_id = " + str(config_sensor_id))
                    if sensor_id == config_sensor_id:
                        local_temperature = loaded[sensor_id][0][0]
                        min_temperature =          temperature_control_config[current_case]["temperature_on"]
                        max_temperature =          temperature_control_config[current_case]["temperature_off"]
                        local_relay_id =           temperature_control_config[current_case]["relay_id"]
                        local_relay_active_state = temperature_control_config[current_case]["relay_active_state"]
                        local_relay_passive_state =temperature_control_config[current_case]["relay_passive_state"]
                        local_relay_error_state =  temperature_control_config[current_case]["relay_error_state"] 
                        local_control =            temperature_control_config[current_case]["control_on_off"] 
                        
                        #print(sensor_id)
                        #print(local_relay_id)

                        if local_control == 1:
                            if type(local_temperature) is str: 
                                if local_temperature == "error":
                                    relay_load= {"relay_id": local_relay_id,"relay_state": local_relay_error_state}
                                    publish.single(relay_set_topic, json.dumps(relay_load), hostname="localhost", auth=mqtt_auth)
                            else:
                                if local_temperature < min_temperature:
                                    relay_load= {"relay_id": local_relay_id,"relay_state": local_relay_active_state}
                                    publish.single(relay_set_topic, json.dumps(relay_load), hostname="localhost", auth=mqtt_auth)
                                if local_temperature > max_temperature:
                                    relay_load= {"relay_id": local_relay_id,"relay_state": local_relay_passive_state}
                                    publish.single(relay_set_topic, json.dumps(relay_load), hostname="localhost", auth=mqtt_auth)





ct_error, topic_config = tools_config.load_config("config_topics.json")
if ct_error != 0:
    print("load config_topic error")
    exit(ct_error)

relay_set_topic = topic_config["digital_output_set_sensors_topic"]
temperature_topic = topic_config["temperature_sensors_topic"]
error_runtime_topic = topic_config["error_runtime_log"]
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set('mqtt_user', 'qwertyui')
client.connect("localhost", 1883, 60)
client.loop_forever()
