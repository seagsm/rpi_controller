#!/usr/bin/python3

import os
import glob
import time
import json
import tools_config
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'




def temperature_sensor_detect(sensor_path):
    temperature_sensor_list = glob.glob(sensor_path + "/28*")
    return temperature_sensor_list

def read_temp_raw(device_folder):
    device_file = device_folder + '/w1_slave'
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temperature_sensor(device_folder):
    local_error = 0
    temp_c = 'error'
    if os.path.isdir(device_folder) == True:
        lines = read_temp_raw(device_folder)
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = float(temp_string) / 1000.0
    else:
        local_error = -1

    return local_error, temp_c

temperature = {}
# 1) Return list of connected temperature sensors:
temp_sensor_list = temperature_sensor_detect(base_dir)
sensors_number = len(temp_sensor_list)

while True:
    if sensors_number > 0:
        for file in temp_sensor_list:
            sens_id = file.split('/')[-1]
            error,local_value = read_temperature_sensor(file)
            temperature[sens_id] = local_value
    print(json.dumps(temperature))
    time.sleep(1)
else:
    exit(error)