#!/usr/bin/python3

import paho.mqtt.publish as publish
import pcf8574_io
import time
import json
import tools_config

# You can use up to 8 PCF8574 boards
# the board will start in input mode
# the pins are HIGH in input mode
p1 = pcf8574_io.PCF(0x27)

# You can use multiple boards with different addresses
#p2 = pcf8574_io.PCF(0x21)

# p0 to p7 are the pins name
# INPUT or OUTPUT is the mode
p1.pin_mode("p0", "INPUT")
p1.pin_mode("p1", "INPUT")
p1.pin_mode("p2", "INPUT")
p1.pin_mode("p3", "INPUT")
p1.pin_mode("p4", "INPUT")
p1.pin_mode("p5", "INPUT")
p1.pin_mode("p6", "INPUT")
p1.pin_mode("p7", "INPUT")

#print(p1.get_all_mode()) # returns list of all pins ["OUTPUT","INPUT",...etc]
digital_input_state = {}
set_topic = ""
error_start_log_topic = "topic/ServiceStartError"


mqtt_auth = { 'username': 'mqtt_user', 'password': 'qwertyui' }


error, topic_config = tools_config.load_config("config_topics.json")
if error != 0:
    error_data = {}
    error_data["service_digital_input"] = "error"
    publish.single(error_start_log_topic,  json.dumps(error_data), hostname="localhost",auth=mqtt_auth)
    exit(error)

error, channels_config = tools_config.load_config("config_digital_in_channels.json")
if error != 0:
    error_data = {}
    error_data["service_digital_input"] = "error"
    publish.single(error_start_log_topic, json.dumps(error_data), hostname="localhost",auth=mqtt_auth)
    exit(error)

error, system_config = tools_config.load_config("config_system.json")
if error != 0:
    error_data = {}
    error_data["service_digital_input"] = "error"
    publish.single(error_start_log_topic, json.dumps(error_data), hostname="localhost",auth=mqtt_auth)
    exit(error)

mqtt_auth = { 'username': system_config['mqtt_user'], 'password': system_config['mqtt_password'] }


while True:
    digital_input_state["input_1"] = [(p1.read("p0"), channels_config["input_1"],system_config["box_id"])]
    digital_input_state["input_2"] = [(p1.read("p1"), channels_config["input_2"],system_config["box_id"])]
    digital_input_state["input_3"] = [(p1.read("p2"), channels_config["input_3"],system_config["box_id"])]
    digital_input_state["input_4"] = [(p1.read("p3"), channels_config["input_4"],system_config["box_id"])]
    digital_input_state["input_5"] = [(p1.read("p4"), channels_config["input_5"],system_config["box_id"])]
    digital_input_state["input_6"] = [(p1.read("p5"), channels_config["input_6"],system_config["box_id"])]
    digital_input_state["input_7"] = [(p1.read("p6"), channels_config["input_7"],system_config["box_id"])]
    digital_input_state["input_8"] = [(p1.read("p7"), channels_config["input_8"],system_config["box_id"])]

    set_topic = topic_config["digital_input_sensors_topic"]
    publish.single(set_topic, json.dumps(digital_input_state), hostname="localhost",auth=mqtt_auth)
    #print(json.dumps(digital_input_state))
    time.sleep(3)

exit(error)
