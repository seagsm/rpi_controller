import os
import json
import logging


def load_config(file_name):
    error = 0
    config = {}
    # Check, if load file exist:
    if os.path.isfile(file_name) == True:
        try:
            with open(file_name) as f:
                config = json.load(f)
                f.close()
                # logging.info("Load configuration file: " + file_name)
        except IOError as e:
            error = -1
            logging.info("Error during reading configuration file: " + file_name)
    else:
        logging.info("Configuration file don't exist: " + file_name)
        error = -1
    return error, config


def save_config(file_name, config):
    error = 0
    try:
        f = open(file_name, 'w')
        json.dump(config, f, sort_keys=True, indent=4)
        f.close()
        # logging.info("Save configuration file: " + file_name)
    except IOError as e:
        error = -1
        logging.info("Error during writing of configuration file: " + file_name)
    return error


