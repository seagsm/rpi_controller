#!/usr/bin/python3

import paho.mqtt.client as mqtt
import pcf8574_io
import time

# You can use up to 8 PCF8574 boards
# the board will start in input mode
# the pins are HIGH in input mode
p1 = pcf8574_io.PCF(0x27)

# You can use multiple boards with different addresses
#p2 = pcf8574_io.PCF(0x21)

# p0 to p7 are the pins name
# INPUT or OUTPUT is the mode
p1.pin_mode("p0", "INPUT")
p1.pin_mode("p1", "INPUT")
p1.pin_mode("p2", "INPUT")
p1.pin_mode("p3", "INPUT")
p1.pin_mode("p4", "INPUT")
p1.pin_mode("p5", "INPUT")
p1.pin_mode("p6", "INPUT")
p1.pin_mode("p7", "INPUT")

print(p1.get_all_mode()) # returns list of all pins ["OUTPUT","INPUT",...etc]

while True:
    print(p1.read("p0"))
    print(p1.read("p1"))
    print(p1.read("p2"))
    print(p1.read("p3"))
    print(p1.read("p4"))
    print(p1.read("p5"))
    print(p1.read("p6"))
    print(p1.read("p7"))
    time.sleep(1)


# You can write and read the output pins
# use HIGH or LOW to set the pin, HIGH is +3.3v LOW is 0v
#p1.pin_mode("p7", "OUTPUT")
#p1.write("p7", "LOW")
#print(p1.read("p7"))

# Additional you can do the following
#p1.set_i2cBus(1)
#p1.get_i2cBus()
#print(p1.get_pin_mode("p7")) # returns string OUTPUT, INPUT
#print(p1.is_pin_output("p7")) # returns boolean True, False
#print(p1.get_all_mode()) # returns list of all pins ["OUTPUT","INPUT",...etc]
