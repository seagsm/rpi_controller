import glob


def temperature_sensor_detect(sensor_path):
    temperature_sensor_list = glob.glob(sensor_path + "/28*")
    return temperature_sensor_list



def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
    mypath = '..'
    listfl = glob.glob(mypath + "/28*")
    print(listfl)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    list_of_sensors = temperature_sensor_detect('..')
    if(len(list_of_sensors) > 0):
        for file in list_of_sensors:
            print(file)

    print("End")


