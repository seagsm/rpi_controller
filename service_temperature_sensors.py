#!/usr/bin/python3

import os
import glob
import time
import paho.mqtt.publish as publish
import json
import logging
import tools_config
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'


def temperature_sensor_detect(sensor_path):
    temperature_sensor_list = glob.glob(sensor_path + "/28*")
    return temperature_sensor_list

def read_temp_raw(device_folder):
    device_file = device_folder + '/w1_slave'
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temperature_sensor(device_folder):
    local_error = 0
    temp_c = 'error'
    if os.path.isdir(device_folder) == True:
        lines = read_temp_raw(device_folder)
#        print (device_folder)
#        print (lines)
#        print ("start read")
        len_lines = len(lines)
        if len_lines > 0:
            while lines[0].strip()[-3:] != 'YES':
                time.sleep(0.2)
                lines = read_temp_raw()
            equals_pos = lines[1].find('t=')
            if equals_pos != -1:
                temp_string = lines[1][equals_pos+2:]
                temp_c = float(temp_string) / 1000.0
    else:
        local_error = -1

    return local_error, temp_c


def save_sensor_list(temp_sensor_list):
    local_error, config = tools_config.load_config("config_temperature_sensor.json")
    if local_error == 0:
        sensors_number = len(temp_sensor_list)
        if sensors_number > 0:
            index = 0
            for file_name in temp_sensor_list:
                sens_id = file_name.split('/')[-1]
                cfg_key = tuple(config.items())[index][0]
                config[cfg_key] = sens_id
                index = index + 1
            tools_config.save_config("config_temperature_sensor.json", config)


temperature = {}
# 1) Return list of connected temperature sensors:
temp_sensor_list = temperature_sensor_detect(base_dir)
sensors_number = len(temp_sensor_list)
# it will create temperature sensors list:
#save_sensor_list(temp_sensor_list)


error, system_config = tools_config.load_config("config_system.json")
if error != 0:
    exit(error)


mqtt_auth = { 'username': system_config['mqtt_user'], 'password': system_config['mqtt_password'] }

error, topic_config = tools_config.load_config("config_topics.json")
if error != 0:
    exit(error)

error, channels_config = tools_config.load_config("config_temperature_channels.json")
if error != 0:
    exit(error)

while True:
    # refresh list of connected sensors:
    temp_sensor_list = temperature_sensor_detect(base_dir)
    # check numbers of connected sensors
    sensors_number = len(temp_sensor_list)
#    print(sensors_number)
    
    if sensors_number > 0:
        # extract temperature value of each sensor from list:
        for file in temp_sensor_list:
            sens_id = file.split('/')[-1]
            error,local_value = read_temperature_sensor(file)
            channel_name = channels_config[sens_id]
            board_id = system_config["box_id"]
            #if local_value != "error":
            temperature[sens_id] = [(local_value, channel_name,board_id)]
        publish.single(topic_config["temperature_sensors_topic"], json.dumps(temperature), hostname = "localhost", auth=mqtt_auth)
 #       print(json.dumps(temperature))
    time.sleep(3)

else:
    exit(error)
